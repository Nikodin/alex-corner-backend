//Configs for express, socket.io.
const express = require('express');
const req = require('express/lib/request');
const app = express();
const http = require('http');
const { emit } = require('process');
const server = http.createServer(app);
const { Server, Socket } = require("socket.io");
const io = new Server(server);

//Adds the middleware required to parse JSON.
app.use(express.json())

//The Stocklist containing our stock data. 
const stocksList = [
  {
      "stockName" : 'ABC Traders',
      "price" : 44.51, 
  },
  {
      "stockName": "Trendy FinTech",
      "price": 23.99,
  },
  {
      "stockName": "SuperCorp",
      "price" : 126.91,
  }
]

//Sends the stockslist data onto /current when probed.   
app.get('/current', (req, res) => {
    res.send(stocksList)
})

//Booting/Sends up our html.index file.
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html');
});



 //Patch /update with the updated data (updatedStock)  
 app.patch('/update', (req, res) => {
    const {data} = req.body
    
//Check the array for a stockName match.
    const found = stocksList.find(element => element["stockName"] === data.stockName);
    //When patched the updated data is emitted to the client side, along with a json response. 
    //If the names match, assign the new values to the object, if not, message "stock does not exist"
    if (found) {
            io.emit('onStockUpdate', data)
            Object.assign(found, data)
            res.status(200).json(found)
        } else {            
            res.status(404).json("stock does not exist")
        }

    })


//Listens and logs the statement when server successfully runs. 
server.listen(3000, () => {
    console.log('Stock Exchange online...')
});

