# **Project: stockExchange**
## *The (not so) Exceptional Stock Exchange*
 
### **About:**
A simple "feed" function to display an updated stock object on an html page through express,  
and socket IO, as to test my cabalities of creating such a feature (hard but fun!).
  
### **How it works:**
The data is being stored on the server side for this test and not through an API. When an update  
to a particular stock is patched through *'/update'* the new data is emitted and retrieved on the client side with  
the particular stock name and price.  

The data is stored in a div-container on the *'/', or front page.*

The list of stocks used in this simulation is stored and can be retrieved through an *GET-request of '/current'*  

The project includes the index.js file containing the server-side function, while the index.html serves as a  
skeleton for the simulation with a not so creative puns as the header! 

Comments are included within the files. 


